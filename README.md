# Composant Discourse pour La Copro Des Possibles

Ce Composant Discourse contient les éléments suivants :

- Font Quicksand-bold
- Font Opensans
- La récupération de ces typographies dans le fichier about.json

Il est installé par le theme [Discourse pour La Copro Des Possibles](https://gitlab.com/coprodespossibles/theme-discourse).

## Utilisation de ce composant

Voir le  [README du theme Discourse pour La Copro Des Possibles](https://gitlab.com/coprodespossibles/theme-discourse#utilisation-de-ce-th%C3%A8me).
